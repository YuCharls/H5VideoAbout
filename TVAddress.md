
>CCTV
>>* CCTV-1高清
http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEdqDZjOQDxRMIrB68w5uI160414/ELMTJYlHrKQaMIltnzDzGfw7160414_1460630822.png
>>* CCTV-2高清
http://ivi.bupt.edu.cn/hls/cctv2hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEGJXtt9IosRPRo2JkaKLr160414/ELMTBuLpRx1rF76N1FUQCvoY160414_1460616309.png
>>* CCTV-3高清
http://ivi.bupt.edu.cn/hls/cctv3hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEbs6B8UI4UiVRrV9FOrhu160415/ELMTQjSbrJSfsvkXVSxmrs2K160415_1460700896.png
>>* CCTV-4高清
http://ivi.bupt.edu.cn/hls/cctv4hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGE0zGSkJyGWSfe7p5hmd2O160414/ELMTpczNhFfnjUTDVZHJBDwt160414_1460623280.png
>>* CCTV-5高清
http://tv.cctv.com/live/cctv5/m/?spm=C28340.PFfibrpzDugF.En59pIUdUZS1.10
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGETEIZZ4GiRbyPpk11xQdL160414/ELMTWHMxJ0d8XhldQZEClglQ160414_1460625135.png
>>* CCTV-5+高清
http://ivi.bupt.edu.cn/hls/cctv5phd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEA7rM0sKrWVSAO0hcwXGK160414/ELMThTOIE4lvUg5gDa6qYf0Z160414_1460628727.png
>>* CCTV-6高清
http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEqfbmcwebnV2G5GOSR0a1160414/ELMT1hb2TZo6DZYMiSI5bCXx160415_1460688436.png
>>* CCTV-7高清
http://ivi.bupt.edu.cn/hls/cctv7hd.m3u8
http://p2.img.cctvpic.com/uploadimg/2019/07/30/1564461292719129.png
>>* CCTV-8高清
http://ivi.bupt.edu.cn/hls/cctv8hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGER9i7AU0AMa2t9CzJTltU160415/ELMTAvdUhtcxBVi7PEBt5RRk160415_1460704800.png
>>* CCTV-9高清
http://ivi.bupt.edu.cn/hls/cctv9hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEVkcgLVeByP5UX9bHs3eH160415/ELMTZmVT3Pfpd5YHeduVKOMG160415_1460709834.jpg
>>* CCTV-10高清
http://ivi.bupt.edu.cn/hls/cctv10hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEvZ9e3BrepuedklC5JxRT160414/ELMTJOlaMQnHhArGrpnFt4eJ160414_1460619399.png
>>* CCTV-11
http://ivi.bupt.edu.cn/hls/cctv11.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEuSNaRsCv3sJp0fqoaSzo160414/ELMTezyF0lKoM39nStxks2br160415_1460685302.png
>>* CCTV-12高清
http://ivi.bupt.edu.cn/hls/cctv12hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGE2nqjXaCFGaP8U89Jw27I160414/ELMTHLyrppf0tVLSR6umKlCB160414_1463551211.png
>>* CCTV-13
http://ivi.bupt.edu.cn/hls/cctv13.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGE1dzdfA9CsHZAlnKJX7NE160113/ELMTFsE3wCrmJXPWgRi9vlhb160113_1459395017.png
>>* CCTV-14高清
http://ivi.bupt.edu.cn/hls/cctv14hd.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEQ1KSin2j2U5FERGWHp1h160415/ELMTnGlKHUJZi7lz19PEnqhM160415_1460715755.png
>>* CCTV-15
http://ivi.bupt.edu.cn/hls/cctv15.m3u8
http://p1.img.cctvpic.com/photoAlbum/templet/special/PAGEO8MkQf6EuyzAGbRG6ONa160414/ELMTBc9qrN6J3IxBmUW7RpD4160414_1460623567.png
>>* CCTV-17高清
http://ivi.bupt.edu.cn/hls/cctv17hd.m3u8
http://p3.img.cctvpic.com/photoAlbum/templet/special/PAGEHP1pnuSayxqIDjzBAovA190710/ELMTRGz3lC8z8vnzmWK8uLvE190710_1564579644.jpg


>卫视频道
>>* 北京卫视高清
http://ivi.bupt.edu.cn/hls/btv1hd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123002151.jpg
>>* 湖南卫视高清
http://ivi.bupt.edu.cn/hls/hunanhd.m3u8
http://hq.ioioz.com/d/file/2014/1014/20141014100423640.jpg
>>* 浙江卫视高清
http://ivi.bupt.edu.cn/hls/zjhd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123051441.jpg
>>* 江苏卫视高清
http://ivi.bupt.edu.cn/hls/jshd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123530799.jpg
>>* 东方卫视高清
http://ivi.bupt.edu.cn/hls/dfhd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417122845956.jpg
>>* 安徽卫视高清
http://ivi.bupt.edu.cn/hls/ahhd.m3u8
http://hq.ioioz.com/d/file/2014/0116/20140116014331377.jpg
>>* 黑龙江卫视高清
http://ivi.bupt.edu.cn/hls/hljhd.m3u8
http://hq.ioioz.com/d/file/2014/0728/20140728092337481.jpg
>>* 吉林卫视高清
http://ivi.bupt.edu.cn/hls/jlhd.m3u8
http://www.tvyan.com/uploads/dianshi/jilintv.jpg
>>* 辽宁卫视高清
http://ivi.bupt.edu.cn/hls/lnhd.m3u8
http://hq.ioioz.com/d/file/2014/0116/20140116014711584.jpg
>>* 深圳卫视高清
http://ivi.bupt.edu.cn/hls/szhd.m3u8
http://hq.ioioz.com/d/file/2014/0101/20140101082127540.jpg
>>* 广东卫视高清
http://ivi.bupt.edu.cn/hls/gdhd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123256441.jpg
>>* 天津卫视高清
http://ivi.bupt.edu.cn/hls/tjhd.m3u8
http://hq.ioioz.com/d/file/2016/1107/b07ac3b17af177a9cbfc42de044e269b.jpg
>>* 湖北卫视高清
http://ivi.bupt.edu.cn/hls/hbhd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123343786.jpg
>>* 山东卫视高清
http://ivi.bupt.edu.cn/hls/sdhd.m3u8
http://www.tvyan.com/uploads/dianshi/shandongtv.jpg
>>* 重庆卫视高清
http://ivi.bupt.edu.cn/hls/cqhd.m3u8
http://hq.ioioz.com/d/file/2018/0517/small0796d1332aadb73c05adfdce6a6ecbce.jpg
>>* 东南卫视高清
http://ivi.bupt.edu.cn/hls/dnhd.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123421472.jpg
>>* 四川卫视高清
http://ivi.bupt.edu.cn/hls/schd.m3u8
http://hq.ioioz.com/d/file/2014/0722/20140722105013368.jpg
>>* 河北卫视高清
http://ivi.bupt.edu.cn/hls/hebhd.m3u8
http://hq.ioioz.com/d/file/2014/0728/20140728091953388.jpg
>>* 河南卫视
http://ivi.bupt.edu.cn/hls/hntv.m3u8
http://hq.ioioz.com/d/file/2014/0116/20140116013947688.jpg
>>* 江西卫视高清
http://ivi.bupt.edu.cn/hls/jxhd.m3u8
http://hq.ioioz.com/d/file/2014/0722/20140722104840995.jpg
>>* 广西卫视高清
http://ivi.bupt.edu.cn/hls/gxhd.m3u8
http://hq.ioioz.com/d/file/2016/1008/8609a72d8c05bfb1d52283eb86554a5c.jpg
>>* 海南卫视高清
http://ivi.bupt.edu.cn/hls/lyhd.m3u8
http://hq.ioioz.com/d/file/2019/0622/smallafea8975ac2891a5620dce337eaaa175.jpg
>>* 贵州卫视高清
http://ivi.bupt.edu.cn/hls/gzhd.m3u8
http://hq.ioioz.com/d/file/2014/0116/20140116013257852.jpg
>>* 西藏卫视
http://ivi.bupt.edu.cn/hls/xztv.m3u8
http://hq.ioioz.com/d/file/2019/0430/small6d95c8ae558a93b175730fef21d6452b1556594081.jpg
>>* 内蒙古卫视
http://ivi.bupt.edu.cn/hls/nmtv.m3u8
http://hq.ioioz.com/d/file/2016/0918/4508a64148d483ba76423102a0e94c7f.jpg
>>* 青海卫视
http://ivi.bupt.edu.cn/hls/qhtv.m3u8
http://hq.ioioz.com/d/file/2016/0918/small0a2c71268d2e230752f374bb5dd1da691474980739.jpg
>>* 厦门卫视
http://ivi.bupt.edu.cn/hls/xmtv.m3u8
http://hq.ioioz.com/d/file/2016/1019/4a991cd85dd7502f614f7b9774210b50.jpg
>>* 新疆卫视
http://ivi.bupt.edu.cn/hls/xjtv.m3u8
http://hq.ioioz.com/d/file/2016/0918/small120325aa990a921f870c82470839952a1474194240.jpg
>>* 云南卫视
http://ivi.bupt.edu.cn/hls/yntv.m3u8
http://hq.ioioz.com/d/file/2015/0417/20150417123156974.jpg
>>* 宁夏卫视
http://ivi.bupt.edu.cn/hls/nxtv.m3u8
http://hq.ioioz.com/d/file/2016/0918/216cc194a526fa24661f5bce9c0754da.jpg
>>* 甘肃卫视
http://ivi.bupt.edu.cn/hls/gstv.m3u8
http://hq.ioioz.com/d/file/2016/1008/86cbea4ba5011f81ec76c9a2d89acb93.jpg
>>* 兵团卫视
http://ivi.bupt.edu.cn/hls/bttv.m3u8
http://hq.ioioz.com/d/file/2016/0911/7b38f2e557f39b45e0420239f65500b9.jpg
>>* 延边卫视
http://ivi.bupt.edu.cn/hls/ybtv.m3u8
http://hq.ioioz.com/d/file/2016/1020/162de1a70b0fd0924285f87d70ff8c2c.jpg
>>* 三沙卫视
http://ivi.bupt.edu.cn/hls/sstv.m3u8
http://hq.ioioz.com/d/file/2016/0911/db61c54f57c1c3b55e81f492bafbaa78.jpg



>其他频道
>>* 北京文艺高清
http://ivi.bupt.edu.cn/hls/btv2hd.m3u8
http://hq.ioioz.com/d/file/2014/0512/20140512033348836.jpg
>>* 北京影视高清
http://ivi.bupt.edu.cn/hls/btv4hd.m3u8
http://hq.ioioz.com/d/file/2014/0512/20140512033629551.jpg
>>* 北京新闻高清
http://ivi.bupt.edu.cn/hls/btv9hd.m3u8
http://hq.ioioz.com/d/file/2014/0512/20140512034324556.jpg
>>* 北京冬奥纪实高清
http://ivi.bupt.edu.cn/hls/btv11hd.m3u8
http://www.tvyan.com/uploads/dianshi/btvdajs.jpg
>>* 北京卡酷少儿
http://ivi.bupt.edu.cn/hls/btv10.m3u8
http://www.tvyan.com/uploads/dianshi/kaku.jpg
>>* CGTN高清
http://ivi.bupt.edu.cn/hls/cgtnhd.m3u8
http://www.tvyan.com/uploads/dianshi/cgtnnews.jpg
>>* CGTN-DOC高清
http://ivi.bupt.edu.cn/hls/cgtndochd.m3u8
http://www.tvyan.com/uploads/dianshi/cgtndocumentary.jpg
>>* CHC高清电影
http://ivi.bupt.edu.cn/hls/chchd.m3u8
http://www.tvyan.com/uploads/dianshi/chchd.jpg

